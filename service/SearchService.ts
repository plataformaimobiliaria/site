import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { MysqlApiSearchService } from './mysqlApi/SearchService'
import { Property } from '~/models/Property'
import PropertyLocation from '@/models/search/PropertyLocation'
import { SearchFilter } from '@/models/search/SearchFilter'

export interface SearchPropertiesResponse {
	temPaginacao: boolean
	qtdItens: number
	qtdTotalItens: number
	pagina: number
	temGeolocalizacao: boolean
	imoveis: Property[]
	imoveisComGeolocalizacao: Property[]
}

export interface SearchOptions {
	opcoes: {
		'cidade': string
		'uf': string
		'endereco': string
		'bairro': string[]
		'tipoImovel': string
		'idReferenciaCRM': string
	}[]
	valorVendaMax:   number
	valorVendaMin:   number
	valorLocacaoMax: number
	valorLocacaoMin: number
}

export interface Lead {
  idImovel: number,
  idAssinante: number,
  idReferenciaCRM: string,
  dataCadastro: string,
  name: string,
  email: string,
  ddi: string,
  ddd: string,
  phone: number,
  message: string
}

export interface InsertLeadResponse {
  lead: Lead,
  whatsapp: string
}

export interface SearchService {
	// Imóveis
	searchPublishedProperties(filter: SearchFilter, $axios: NuxtAxiosInstance): Promise<SearchPropertiesResponse>
	getPropertyById(id: string, $axios: NuxtAxiosInstance): Promise<Property>
	getAvailableLocations($axios: NuxtAxiosInstance): Promise<PropertyLocation[]>
	getSearchOptions($axios: NuxtAxiosInstance): Promise<SearchOptions>
	insertLead(leadValues: Lead, $axios: NuxtAxiosInstance): Promise<InsertLeadResponse>
}

function NewSearchService(provider: string): SearchService {
	switch (provider) {
		case 'apiImobiliaria':
			return new MysqlApiSearchService()
		default:
			throw new Error('Provedor de serviço de imóveis não implementado')
	}
}

export default NewSearchService('apiImobiliaria')
