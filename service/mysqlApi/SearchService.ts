/* eslint-disable import/namespace */
import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { InsertLeadResponse, Lead, SearchOptions, SearchPropertiesResponse, SearchService } from '../SearchService'
import { SearchFilter } from 'models/search/SearchFilter'
import { BusinessTypes, Property, PropertyCRM, searchPropertiesResponseCRM, PropertyTypes, PropertyZones } from '~/models/Property'
import PropertyLocation from 'models/search/PropertyLocation'

export class MysqlApiSearchService implements SearchService {
	async searchPublishedProperties(filter: SearchFilter, $axios: NuxtAxiosInstance): Promise<SearchPropertiesResponse> {
		let response: searchPropertiesResponseCRM

		try {
			const axiosResponse = await $axios.request<searchPropertiesResponseCRM>({
				method: 'GET',
				url: 'select-imovel',
				params: {
					referencia: filter.referencia,
					temGeolocalizacao: true,
					temPaginacao: true,
					qtdItens: filter.qntItens,
					pagina: filter.page,
					uf: filter.states || [''],
					cidade: filter.cities || [''],
          bairro: filter.neighborhoods || [''],
					qtdBedrooms: filter.bedrooms,
					suite: filter.suite,
					banheiro: filter.toilet,
					vagasGaragem: filter.car,
					check: filter.checkSelected,
					tipoNegocio: bussinessTypeToTipoNegocio(filter.businessType),
					tipoImovel: filter.propertyTypes,
					vendaMax: filter.priceRange[1],
					vendaMin: filter.priceRange[0],
					filtroAlterado: filter.changedFilter,
				},
				headers: {'Content-Type': 'application/json'},
			})
			response = axiosResponse.data
			if (response.imoveis === null){
				throw new Error('Imóvel não encontrado')
			}
		} catch (error) {
			return {temPaginacao: false, temGeolocalizacao: false, qtdItens: 0, qtdTotalItens: 0, pagina: 1, imoveisComGeolocalizacao: [], imoveis: []}
		}

		const properties:Property[] = response.imoveis.map((propertyCRM:PropertyCRM) => {

			// Verifica se existe
			if (!propertyCRM.Imagens){
				propertyCRM.Imagens = []
			}

			const property: Property = {
				// ID
				id: propertyCRM.id,
				idCRM: propertyCRM.idReferenciaCRM,
				// Localização
				zone: finalidadeCRMparaPropertyZones(propertyCRM.finalidade),
				type: tipoImovelCRMparaPropertyType(propertyCRM.tipoImovel),
				addressLine: propertyCRM.endereco,
				neighborhood: propertyCRM.bairro,
				city: propertyCRM.cidade,
				state: propertyCRM.uf,
				// Area do Imovel
				landArea: propertyCRM.areaTotal,
				// Caracteriscas
				propertySituation: propertyCRM.estadoDoImovel,
				bedrooms: propertyCRM.qtdeDormitorios,
				quantityBathroom: propertyCRM.qtdeBanheiro,
				vacanciesGarage: propertyCRM.vagasGaragem,
				suites: propertyCRM.suite,
				furnished: propertyCRM.mobiliado,
				porch: propertyCRM.varanda,
				water: propertyCRM.agua,
				reformed: propertyCRM.reformado,
				semiFurnished: propertyCRM.semiMobiliado,						// semiMobiliado
				plannedFurniture: propertyCRM.moveisPlanejados,					// moveisPlanejados
				decorated: propertyCRM.decorado,								// decorado
				mezzanine: propertyCRM.mezanino,								// mezanino
				balcony: propertyCRM.sacada,									// sacada
				balconyBackground:  propertyCRM.sacadaFundo,					// sacadaFundo
				sideBalcony: propertyCRM.sacadaLateral, 						// sacadaLateral
				airConditioner: propertyCRM.arCondicionado,												// arCondicionado
				closetRoom: propertyCRM.armarioSala, 													// armarioSala
				closetBathroom: propertyCRM.armarioBanheiro,										// armarioBanheiro
				wardrobeCloset: propertyCRM.armarioCloset,										// armarioCloset
				cabinetKitchen: propertyCRM.armarioCozinha,										// armarioCozinha
				wardrobeDormitory: propertyCRM.armarioDormitorio,												// armarioDormitorio
				porcelainTile: propertyCRM.porcelanato,											// porcelanato
				carpet: propertyCRM.carpete,												// carpete
				subfloor: propertyCRM.contraPiso,												// contraPiso
				granite: propertyCRM.granito,												// granito
				laminate: propertyCRM.laminado,												// laminado
				marble: propertyCRM.marmore,												// marmore
				coldFloor: propertyCRM.pisoFrio,												// pisoFrio
				floorCeramics: propertyCRM.pisoCeramica,											// pisoCeramica
				peg: propertyCRM.taco,													// taco
				hydroMassage: propertyCRM.hidroMassagem,											// hidroMassagem
				steamRoom: propertyCRM.sauna,												// sauna
				privateSauna: propertyCRM.saunaPrivada,											// saunaPrivada
				hotTub: propertyCRM.ofuro,													// ofuro
				spa: propertyCRM.spa,											// spa
				fireplace: propertyCRM.lareira,							// lareira
				wineHouse: propertyCRM.adega,								// adega
				pub: propertyCRM.bar,											// bar
				alarm: propertyCRM.alarme,										// alarme
				hotWater: propertyCRM.aguaQuente,								// aguaQuente
				centralHeater: propertyCRM.aquecedorCentral,						// aquecedorCentral
				solarHeater: propertyCRM.aquecedorSolar,							// aquecedorSolar
				carpetWood: propertyCRM.carpeteMadeira,							// carpeteMadeira
				circuitTV: propertyCRM.circuitoTV,								// circuitoTV
				areaGourmet: propertyCRM.areaGourmet,							// areaGourmet
				walled: propertyCRM.murado,									// murado
				roomMovie: propertyCRM.salaCinema,								// salaCinema
				oven: propertyCRM.forno,											// forno
				houseHomemade: propertyCRM.casaCaseiro,						// casaCaseiro
				highVoltageNetwork: propertyCRM.redeAltaTensao,			// redeAltaTensao
				waterfall: propertyCRM.cachoeira,								// cachoeira
				barn: propertyCRM.celeiro,											// celeiro
				fence: propertyCRM.cerca,									// cerca
				corral: propertyCRM.curral,										// curral
				lake: propertyCRM.lago,											// lago
				pasture: propertyCRM.pasto,									// pasto
				wellFish: propertyCRM.pocoPeixe,									// pocoPeixe
				waterReservoir: propertyCRM.reservatorioAgua,						// reservatorioAgua
				river: propertyCRM.rio,										// rio
				transformer: propertyCRM.transformador,							// transformador
				exchange: propertyCRM.permuta,								// permuta
				financedProperty: propertyCRM.imovelFinanciado,				// imovelFinanciado
				quantityRoom: propertyCRM.qtdeSala,							// qtdeSala
				quantityLivingRoom: propertyCRM.qtdeSalaEstar,				// qtdeSalaEstar
				rightFootFootage: propertyCRM.metragemPeDireito,					// metragemPeDireito
				pantry: propertyCRM.copa, 									// copa
				kitchen: propertyCRM.cozinha,									// cozinha
				americanKitchen: propertyCRM.cozinhaAmericana,					// cozinhaAmericana
				dependentKitchen: propertyCRM.cozinhaDependente,			// cozinhaDependente
				plannedKitchen: propertyCRM.cozinhaPlanejada,					// cozinhaPlanejada
				quantityBedrooms: propertyCRM.qtdeDormitorios,					// qtdeDormitorios
				diningRoom: propertyCRM.salaDeJantar,								// salaDeJantar
				office: propertyCRM.escritorio,										// escritorio
				lavatory: propertyCRM.lavabo,								// lavabo
				closet: propertyCRM.closet,									// closet
				suiteCloset: propertyCRM.suiteCloset,							// suiteCloset
				suiteMaster: propertyCRM.suiteMaster,							// suiteMaster
				dormitoryMaid: propertyCRM.dormitorioEmpregada,						// dormitorioEmpregada
				bathroomMaid: propertyCRM.banheiroEmpregada,							// banheiroEmpregada
				laundry: propertyCRM.lavanderia,									// lavanderia
				areaLeisure: propertyCRM.areaLazer,							// areaLazer
				barbecueGrill: propertyCRM.churrasqueira,					// churrasqueira
				pool: propertyCRM.piscina,											// piscina
				yard: propertyCRM.quintal,											// quintal
				garden: propertyCRM.jardim,										// jardim
				shed: propertyCRM.edicula,											// edicula
				unitMeasure: propertyCRM.unidadeMedida,							// unidadeMedida
				areaLand: propertyCRM.areaTerreno,									// areaTerreno
				areaBuilt: propertyCRM.areaConstruida,								// areaConstruida
				areaCommon: propertyCRM.areaComum,								// areaComum
				areaService: propertyCRM.areaServico,							// areaServico
				areaUseful: propertyCRM.areaUtil,								// areaUtil
				areaTotal: propertyCRM.areaTotal,								// areaTotal
				valueOnRequest: propertyCRM.valorSobConsulta,					// valorSobConsulta
				valueSale: propertyCRM.valorVenda,								// valorVenda
				valueLocation: propertyCRM.valorLocacao,						// valorLocacao
				valueCondominium: propertyCRM.valorCondominio,					// valorCondominio
				valueIPTU: propertyCRM.valorIPTU,								// valorIPTU
				valueSeasonalRent: propertyCRM.valorAluguelTemporada,				// valorAluguelTemporada
				valuePromotional: propertyCRM.valorPromocional,					// valorPromocional
				priceAvarageSale: propertyCRM.precoMedioVenda,					// precoMedioVenda
				priceAvarageLocation: propertyCRM.precoMedioLocacao,			// precoMedioLocacao


				// Informações
				title: propertyCRM.tituloImovel,
				businessType: vendaLocacaoParaBusinessType(propertyCRM.venda, propertyCRM.locacao),
				propertyInHighlight: propertyCRM.imovelEmDestaque,
				description: propertyCRM.descricaoImovel,
				// Valores
				values:{
					rent: propertyCRM.valorLocacao,
					sellingValue: propertyCRM.valorVenda,
					propertyValue: propertyCRM.valorVenda,
					taxes: propertyCRM.valorIPTU,
					condominiumFee: propertyCRM.valorCondominio
				},
				// Imagens
				images: propertyCRM.Imagens.map((imagemCRM) => {
					return {
						name: imagemCRM.descricaoFoto,
						url: imagemCRM.urlFoto,
						highlight: imagemCRM.fotoPrincipal
					}
				}),
				// GeoLocalização
				geolocation: {
					lat: Number(propertyCRM.latitude),
					lon: Number(propertyCRM.longitude),
				}
			}
			return property
		})

		properties.filter((property) => {
			return !!property.id
		})

		const imoveisComGeolocalizacao:Property[] = response.imoveisComGeolocalizacao.map((propertyCRM:PropertyCRM) => {
				// Verifica se existe
				if (!propertyCRM.Imagens){
					propertyCRM.Imagens = []
				}
				const property: Property = {
					// ID
					id: propertyCRM.id,
					idCRM: propertyCRM.idReferenciaCRM,
					address: propertyCRM.endereco,
					neighborhood: propertyCRM.bairro,
					city: propertyCRM.cidade,
					state: propertyCRM.uf,
					valueSale: propertyCRM.valorVenda,								// valorVenda
					valueLocation: propertyCRM.valorLocacao,						// valorLocacao
					// Informações
					title: propertyCRM.tituloImovel,
					businessType: vendaLocacaoParaBusinessType(propertyCRM.venda, propertyCRM.locacao),
					description: propertyCRM.descricaoImovel,
					type: tipoImovelCRMparaPropertyType(propertyCRM.tipoImovel),
					areaLand: propertyCRM.areaTotal,
					unitMeasure: propertyCRM.unidadeMedida,
					quantityBedrooms: propertyCRM.qtdeDormitorios,
					vacanciesGarage: propertyCRM.vagasGaragem,
					// Valores
					values:{
						rent: propertyCRM.valorLocacao,
						sellingValue: propertyCRM.valorVenda,
						propertyValue: propertyCRM.valorVenda,
						taxes: propertyCRM.valorIPTU,
						condominiumFee: propertyCRM.valorCondominio
					},
					// Imagens
					images: propertyCRM.Imagens.map((imagemCRM) => {
						return {
							name: imagemCRM.descricaoFoto,
							url: imagemCRM.urlFoto,
							highlight: imagemCRM.fotoPrincipal
						}
					}),
					// GeoLocalização
					geolocation: {
						lat: Number(propertyCRM.latitude),
						lon: Number(propertyCRM.longitude),
					}
				}
				return property
			})

		const propertyResponse: SearchPropertiesResponse = {
			imoveis: properties,
			imoveisComGeolocalizacao,
			pagina: response.pagina,
			temGeolocalizacao: response.temGeolocalizacao,
			qtdTotalItens: response.qtdTotalItens,
			qtdItens: response.qtdItens,
			temPaginacao: response.temPaginacao
		}

		return propertyResponse
	}

	async getPropertyById(idImovel: string, $axios: NuxtAxiosInstance): Promise<Property> {
		let propertyCRM:PropertyCRM | null = null
		try {
			const response = await $axios.request<searchPropertiesResponseCRM>({
				method: 'GET',
				url: 'select-imovel',
				params: {referencia: idImovel},
				headers: {'Content-Type': 'application/json'},
			})
			propertyCRM = response.data.imoveis[0] || null
		} catch (error) {
			console.error(error)
		}

		// Verifica se é nulo
		if(propertyCRM === null){
			return Promise.reject(Error('Null property CRM'))
		}

		// Verifica se existe
		if (!propertyCRM.Imagens){
			propertyCRM.Imagens = []
		}

		const property: Property = {
			// ID
			id: propertyCRM.id,
			idCRM: propertyCRM.idReferenciaCRM,
			// Localização
			zone: finalidadeCRMparaPropertyZones(propertyCRM.finalidade),
			type: tipoImovelCRMparaPropertyType(propertyCRM.tipoImovel),
			addressLine: propertyCRM.endereco,
			neighborhood: propertyCRM.bairro,
			city: propertyCRM.cidade,
			state: propertyCRM.uf,
			// Area do Imovel
			landArea: propertyCRM.areaTotal,
			// Caracteriscas
			propertySituation: propertyCRM.estadoDoImovel,
			bedrooms: propertyCRM.qtdeDormitorios,
			quantityBathroom: propertyCRM.qtdeBanheiro,
			vacanciesGarage: propertyCRM.vagasGaragem,
			suites: propertyCRM.suite,
			furnished: propertyCRM.mobiliado,
			porch: propertyCRM.varanda,
			water: propertyCRM.agua,
			reformed: propertyCRM.reformado,
			semiFurnished: propertyCRM.semiMobiliado,						// semiMobiliado
			plannedFurniture: propertyCRM.moveisPlanejados,					// moveisPlanejados
			decorated: propertyCRM.decorado,								// decorado
			mezzanine: propertyCRM.mezanino,								// mezanino
			balcony: propertyCRM.sacada,									// sacada
			balconyBackground:  propertyCRM.sacadaFundo,					// sacadaFundo
			sideBalcony: propertyCRM.sacadaLateral, 						// sacadaLateral
			airConditioner: propertyCRM.arCondicionado,												// arCondicionado
			closetRoom: propertyCRM.armarioSala, 													// armarioSala
			closetBathroom: propertyCRM.armarioBanheiro,										// armarioBanheiro
			wardrobeCloset: propertyCRM.armarioCloset,										// armarioCloset
			cabinetKitchen: propertyCRM.armarioCozinha,										// armarioCozinha
			wardrobeDormitory: propertyCRM.armarioDormitorio,												// armarioDormitorio
			porcelainTile: propertyCRM.porcelanato,											// porcelanato
			carpet: propertyCRM.carpete,												// carpete
			subfloor: propertyCRM.contraPiso,												// contraPiso
			granite: propertyCRM.granito,												// granito
			laminate: propertyCRM.laminado,												// laminado
			marble: propertyCRM.marmore,												// marmore
			coldFloor: propertyCRM.pisoFrio,												// pisoFrio
			floorCeramics: propertyCRM.pisoCeramica,											// pisoCeramica
			peg: propertyCRM.taco,													// taco
			hydroMassage: propertyCRM.hidroMassagem,											// hidroMassagem
			steamRoom: propertyCRM.sauna,												// sauna
			privateSauna: propertyCRM.saunaPrivada,											// saunaPrivada
			hotTub: propertyCRM.ofuro,													// ofuro
			spa: propertyCRM.spa,											// spa
			fireplace: propertyCRM.lareira,							// lareira
			wineHouse: propertyCRM.adega,								// adega
			pub: propertyCRM.bar,											// bar
			alarm: propertyCRM.alarme,										// alarme
			hotWater: propertyCRM.aguaQuente,								// aguaQuente
			centralHeater: propertyCRM.aquecedorCentral,						// aquecedorCentral
			solarHeater: propertyCRM.aquecedorSolar,							// aquecedorSolar
			carpetWood: propertyCRM.carpeteMadeira,							// carpeteMadeira
			circuitTV: propertyCRM.circuitoTV,								// circuitoTV
			areaGourmet: propertyCRM.areaGourmet,							// areaGourmet
			walled: propertyCRM.murado,									// murado
			roomMovie: propertyCRM.salaCinema,								// salaCinema
			oven: propertyCRM.forno,											// forno
			houseHomemade: propertyCRM.casaCaseiro,						// casaCaseiro
			highVoltageNetwork: propertyCRM.redeAltaTensao,			// redeAltaTensao
			waterfall: propertyCRM.cachoeira,								// cachoeira
			barn: propertyCRM.celeiro,											// celeiro
			fence: propertyCRM.cerca,									// cerca
			corral: propertyCRM.curral,										// curral
			lake: propertyCRM.lago,											// lago
			pasture: propertyCRM.pasto,									// pasto
			wellFish: propertyCRM.pocoPeixe,									// pocoPeixe
			waterReservoir: propertyCRM.reservatorioAgua,						// reservatorioAgua
			river: propertyCRM.rio,										// rio
			transformer: propertyCRM.transformador,							// transformador
			exchange: propertyCRM.permuta,								// permuta
			financedProperty: propertyCRM.imovelFinanciado,				// imovelFinanciado
			quantityRoom: propertyCRM.qtdeSala,							// qtdeSala
			quantityLivingRoom: propertyCRM.qtdeSalaEstar,				// qtdeSalaEstar
			rightFootFootage: propertyCRM.metragemPeDireito,					// metragemPeDireito
			pantry: propertyCRM.copa, 									// copa
			kitchen: propertyCRM.cozinha,									// cozinha
			americanKitchen: propertyCRM.cozinhaAmericana,					// cozinhaAmericana
			dependentKitchen: propertyCRM.cozinhaDependente,			// cozinhaDependente
			plannedKitchen: propertyCRM.cozinhaPlanejada,					// cozinhaPlanejada
			quantityBedrooms: propertyCRM.qtdeDormitorios,					// qtdeDormitorios
			diningRoom: propertyCRM.salaDeJantar,								// salaDeJantar
			office: propertyCRM.escritorio,										// escritorio
			lavatory: propertyCRM.lavabo,								// lavabo
			closet: propertyCRM.closet,									// closet
			suiteCloset: propertyCRM.suiteCloset,							// suiteCloset
			suiteMaster: propertyCRM.suiteMaster,							// suiteMaster
			dormitoryMaid: propertyCRM.dormitorioEmpregada,						// dormitorioEmpregada
			bathroomMaid: propertyCRM.banheiroEmpregada,							// banheiroEmpregada
			laundry: propertyCRM.lavanderia,									// lavanderia
			areaLeisure: propertyCRM.areaLazer,							// areaLazer
			barbecueGrill: propertyCRM.churrasqueira,					// churrasqueira
			pool: propertyCRM.piscina,											// piscina
			yard: propertyCRM.quintal,											// quintal
			garden: propertyCRM.jardim,										// jardim
			shed: propertyCRM.edicula,											// edicula
			unitMeasure: propertyCRM.unidadeMedida,							// unidadeMedida
			areaLand: propertyCRM.areaTerreno,									// areaTerreno
			areaBuilt: propertyCRM.areaConstruida,								// areaConstruida
			areaCommon: propertyCRM.areaComum,								// areaComum
			areaService: propertyCRM.areaServico,							// areaServico
			areaUseful: propertyCRM.areaUtil,								// areaUtil
			areaTotal: propertyCRM.areaTotal,								// areaTotal
			valueOnRequest: propertyCRM.valorSobConsulta,					// valorSobConsulta
			valueSale: propertyCRM.valorVenda,								// valorVenda
			valueLocation: propertyCRM.valorLocacao,						// valorLocacao
			valueCondominium: propertyCRM.valorCondominio,					// valorCondominio
			valueIPTU: propertyCRM.valorIPTU,								// valorIPTU
			valueSeasonalRent: propertyCRM.valorAluguelTemporada,				// valorAluguelTemporada
			valuePromotional: propertyCRM.valorPromocional,					// valorPromocional
			priceAvarageSale: propertyCRM.precoMedioVenda,					// precoMedioVenda
			priceAvarageLocation: propertyCRM.precoMedioLocacao,			// precoMedioLocacao


			// Informações
			title: propertyCRM.tituloImovel,
			businessType: vendaLocacaoParaBusinessType(propertyCRM.venda, propertyCRM.locacao),
			propertyInHighlight: propertyCRM.imovelEmDestaque,
			description: propertyCRM.descricaoImovel,
			// Valores
			values:{
				rent: propertyCRM.valorLocacao,
				sellingValue: propertyCRM.valorVenda,
				propertyValue: propertyCRM.valorVenda,
				taxes: propertyCRM.valorIPTU,
				condominiumFee: propertyCRM.valorCondominio
			},
			// Imagens
			images: propertyCRM.Imagens.map((imagemCRM) => {
				return {
					name: imagemCRM.descricaoFoto,
					url: imagemCRM.urlFoto,
					highlight: imagemCRM.fotoPrincipal
				}
			}),
			// GeoLocalização
			geolocation: {
				lat: Number(propertyCRM.latitude),
				lon: Number(propertyCRM.longitude),
			}
		}
		return property
	}

	// eslint-disable-next-line require-await
	async getAvailableLocations(_: NuxtAxiosInstance): Promise<PropertyLocation[]> {
		return Promise.reject(Error('deu pau'))
	}

	async getSearchOptions($axios: NuxtAxiosInstance): Promise<SearchOptions> {
		let response: SearchOptions
		try {
			const axiosResponse = await $axios.request<SearchOptions>({
				method: 'GET',
				url: 'select-opcoes-filtro',
				headers: {'Content-Type': 'application/json'},
			})
			response = axiosResponse.data as SearchOptions
		} catch (error) {
			console.error(error)
			return Promise.reject(Error('deu pau'))
		}
		return response
	}

	async insertLead(leadValues:Lead, $axios: NuxtAxiosInstance): Promise<InsertLeadResponse>{
		let response: InsertLeadResponse

    // &univen_lead=true
		try {
			const axiosResponse = await $axios.post<InsertLeadResponse>('insert-lead?redirect_whatsapp=true&univen_lead=true',{
				idImovel: leadValues.idImovel,
        idAssinante: leadValues.idAssinante,
        idReferenciaCRM: leadValues.idReferenciaCRM,
				dataCadastro: leadValues.dataCadastro,
				name: leadValues.name,
        email: leadValues.email,
        ddi: leadValues.ddi,
				ddd: leadValues.ddd,
				phone: leadValues.phone,
        message: leadValues.message
			})
			response = axiosResponse.data
			console.log('insertLead: ', response)
		} catch (error) {
			console.error(error)
			return Promise.reject(Error('deu pau'))
		}

		return response
	}
}

function tipoImovelCRMparaPropertyType(tipoImovel:string){
	switch (tipoImovel){
		case 'APARTAMENTO':
			return PropertyTypes.APARTMENT
		case 'CASA':
			return PropertyTypes.HOUSE
		case 'CHÁCARA':
			return PropertyTypes.COUNTRYHOUSE
		case 'FAZENDA':
			return PropertyTypes.FARM
		case 'SÍTIO':
			return PropertyTypes.PLACE
		case 'TERRENO':
			return PropertyTypes.LAND
		case 'LOTE':
			return PropertyTypes.LOT
		case 'PONTO':
			return PropertyTypes.POINT
		case 'SALA':
			return PropertyTypes.ROOM
		default:
			return PropertyTypes.HOUSE
	}
}

function finalidadeCRMparaPropertyZones(finalidade:string){
	switch (finalidade){
		case 'RURAL':
			return PropertyZones.RURAL
		case 'RESIDENCIAL':
			return PropertyZones.URBAN
		default:
			return PropertyZones.URBAN
	}
}

function vendaLocacaoParaBusinessType(venda: boolean, locacao: boolean){
	if (venda === true && locacao === true){
		return BusinessTypes.BOTH
	} else if (venda === false && locacao === true){
		return BusinessTypes.RENT
	} else if (venda === true && locacao === false){
		return BusinessTypes.SALE
	} else {
		return BusinessTypes.BOTH
	}
}

function bussinessTypeToTipoNegocio(bussiness?:BusinessTypes){
	if (bussiness === BusinessTypes.RENT) {
		return 'locacao'
	} else if (bussiness === BusinessTypes.SALE) {
		return 'venda'
	} else {
		return ''
	}
}
