export type City = {
	name: string
	neighborhoods: []
}

export type Neighbourhood = {
	name: string[]
}

export type Comodos = {
	room?: number
	suite?: number,
	toilet?: number,
	car?: number,
	checkComodos?: boolean
}

export default class PropertyLocation {
	cities: City[]
	name: string
	state: string

	constructor(cities: any, name: string, state: string) {
		this.cities = cities
		this.name = name
		this.state = state
	}
}
