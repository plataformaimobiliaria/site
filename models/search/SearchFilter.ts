import { BusinessTypes } from '../Property'

export interface ComodosSearchFilter {
	bedrooms?: number
	suite?: number,
	toilet?: number,
	car?: number,
	checkSelected?: boolean
	changedFilter?: boolean
}


export interface SearchFilter extends ComodosSearchFilter {
	businessType?: BusinessTypes
	propertyTypes?: string[]
	priceRange: number[]
	constructedArea?: number
	limit?: number
	states?: string[]
	cities?: string[]
	neighborhoods?: string[]
	page?: number
	qntItens?: number
	referencia?: string
	changedFilter?: boolean
}

export interface ActiveFilters {
  idReferenciaCRM: boolean
  propertyTypes: boolean
  states: boolean
  cities: boolean
  neighborhoods: boolean
  qtdActiveFilters: number
}

