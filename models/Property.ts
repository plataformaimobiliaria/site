export type chips = string[]

export enum PropertyTypes {
	LAND = 'Terreno',
	HOUSE = 'Casa',
	LOFT = 'loft',
	APARTMENT = 'Apartamento',
	STUDIO = 'studio',
	FARM = 'Fazenda',
	FARMSTEAD = 'farmstead',
	COUNTRYHOUSE = 'Chácara',
	PLACE = 'Sítio',
	LOT = 'Lote',
	POINT = 'Ponto Comercial',
	ROOM = 'Sala Comercial'
}

export enum PropertyZones {
	RURAL = 'Rural',
	URBAN = 'Urbano'
}

export enum BusinessTypes {
	RENT = 'Aluguel',
	SALE = 'Venda',
	BOTH = 'Ambos'
}

export enum PropertyStatus {
	RENTED = 'Alugado',
	SOLD = 'Vendido',
	PUBLISHED = 'Publicado',
	REGISTERED = 'Registrado',
	PENDING = 'Pendente',
	INACTIVE = 'Inativo'
}

export interface PropertyImage {
	name: string
	url: string
	highlight: boolean
}

export interface PropertyImageCRM {
	idImovel?: number,
	idReferenciaCRM?: string,
	urlFoto: string,
	descricaoFoto: string,
	fotoPrincipal: boolean
}

export interface PropertyLocation {
	zipCode: string
	country: string
	state: string
	city: string
	neighborhoods: string[]
	complement: string
	address: string
	number: string
}

export interface GeoLocation {
	lat: number
	lon: number
}

export interface Property {
	[key: string]: any
	// IDs
	id?: number 											// id
	idCRM?: string 										// idReferenciaCRM
	idImob?: number 									// idImobiliaria

	// Imovel
	pickup?: string 									// captador
	dateRegister?: string							// dataCadastro
	dateExclusion?: string						// dataExclusao
	contractExclusivity?: boolean			// contratoExclusividade
	businessType?: BusinessTypes			// venda || locacao
	exchange?: boolean								// permuta
	financedProperty?: boolean				// imovelFinanciado
	acceptFinancing?: boolean					// aceitaFinanciamento
	propertyInHighlight?: boolean			// imovelEmDestaque
	type?: PropertyTypes							// tipoImovel
	zone?: PropertyZones							// finalidade
	yearConstruction?: number					// anoConstrucao
	dateRenovation?: number						// dataReforma
	propertyFace?: string							// faceDoImovel
	quantityRoom?: number							// qtdeSala
	quantityLivingRoom?: number				// qtdeSalaEstar
	rightFootFootage?: number					// metragemPeDireito
	pantry?: boolean 									// copa
	kitchen?: boolean									// cozinha
	americanKitchen?: boolean					// cozinhaAmericana: sim
	dependentKitchen?: boolean				// cozinhaDependente
	plannedKitchen?: boolean					// cozinhaPlanejada
	quantityBedrooms?: number					// qtdeDormitorios
	diningRoom?: number								// salaDeJantar
	office?: number										// escritorio
	quantityBathroom?: number					// qtdeBanheiro
	lavatory?: number									// lavabo
	suites?: number										// suite
	closet?: number										// closet
	suiteCloset?: number							// suiteCloset
	suiteMaster?: number							// suiteMaster
	dormitoryMaid?: number						// dormitorioEmpregada
	bathroomMaid?: number							// banheiroEmpregada
	laundry?: number									// lavanderia
	vacanciesGarage?: number					// vagasGaragem
	areaLeisure?: number							// areaLazer
	barbecueGrill?: number						// churrasqueira
	pool?: number											// piscina
	yard?: number											// quintal
	garden?: number										// jardim
	shed?: number											// edicula
	unitMeasure?: string							// unidadeMedida
	areaLand?: number									// areaTerreno
	areaBuilt?: number								// areaConstruida
	areaCommon?: number								// areaComum
	areaService?: number							// areaServico
	areaUseful?: number								// areaUtil
	areaTotal?: number								// areaTotal
	title?: string										// tituloImovel
	subTitle?: string									// subTituloImovel
	description?: string							// descricaoImovel
	typeCurrency?: string							// tipoMoeda
	valueOnRequest?: number						// valorSobConsulta
	valueSale?: number								// valorVenda
	valueLocation?: number 						// valorLocacao
	valueCondominium?: number					// valorCondominio
	valueIPTU?: number								// valorIPTU
	valueSeasonalRent?: number				// valorAluguelTemporada
	valuePromotional?: number					// valorPromocional
	priceAvarageSale?: number					// precoMedioVenda
	priceAvarageLocation?: number			// precoMedioLocacao
	descriptionSEO?: string						// descricaoSEO
	keywordsSEO?: string							// palavrasChaveSEO
	propertySituation?: string				// estadoDoImovel

	// Caracteristicas
	reformed?: boolean								// reformado
	furnished?: boolean								// mobiliado
	semiFurnished?: boolean						// semiMobiliado
	plannedFurniture?: boolean				// moveisPlanejados
	decorated?: boolean								// decorado
	porch?: number										// varanda
	mezzanine?: number								// mezanino
	balcony?: number									// sacada
	balconyBackground?: number				// sacadaFundo
	sideBalcony?: number							// sacadaLateral
	airConditioner?: number						// arCondicionado
	closetRoom?: boolean							// armarioSala
	closetBathroom?: boolean					// armarioBanheiro
	wardrobeCloset?: boolean					// armarioCloset
	cabinetKitchen?: boolean					// armarioCozinha
	wardrobeDormitory?: boolean				// armarioDormitorio
	porcelainTile?: boolean						// porcelanato
	carpet?: boolean									// carpete
	subfloor?: boolean								// contraPiso
	granite?: boolean									// granito
	laminate?: boolean								// laminado
	marble?: boolean									// marmore
	coldFloor?: boolean								// pisoFrio
	floorCeramics?: boolean						// pisoCeramica
	peg?: boolean											// taco
	hydroMassage?: number							// hidroMassagem
	steamRoom?: number								// sauna
	privateSauna?: number							// saunaPrivada
	hotTub?: number										// ofuro
	spa?: number											// spa
	fireplace?: number								// lareira
	wineHouse?: number								// adega
	pub?: number											// bar
	alarm?: boolean										// alarme
	water?: boolean										// agua
	hotWater?: boolean								// aguaQuente
	centralHeater?: boolean						// aquecedorCentral
	solarHeater?: boolean							// aquecedorSolar
	carpetWood?: boolean							// carpeteMadeira
	circuitTV?: boolean								// circuitoTV
	areaGourmet?: number							// areaGourmet
	walled?: boolean									// murado
	roomMovie?: number								// salaCinema
	oven?: number											// forno
	houseHomemade?: number						// casaCaseiro
	highVoltageNetwork?: boolean			// redeAltaTensao
	waterfall?: number								// cachoeira
	barn?: number											// celeiro
	fence?: boolean										// cerca
	corral?: number										// curral
	lake?: number											// lago
	pasture?: boolean									// pasto
	wellFish?: number									// pocoPeixe
	waterReservoir?: number						// reservatorioAgua
	river?: boolean										// rio
	transformer?: boolean							// transformador

	// Edificio
	constructionCompany?: string  		// nomeConstrutora
	edificeName?: string							// nomeEdificio
	enterpriseName?: string						// nomeEmpreendimento
	condominiumName?: string					// nomeCondominio
	payCondominium?: boolean					// pagaCondominio
	reception?: boolean								// recepcao
	concierge24Hours?: boolean				// portaria24Horas
	securityCabin?: boolean						// guarita
	electronicDoor?: boolean					// portaoEletronico
	intercom?: boolean								// interfone
	hallEntrance?: boolean						// hallEntrada
	caretaker?: boolean								// zelador
	quantityFloors?: number						// qtdeAndares
	quantityApartmPerFloor?: number		// qtdeAptoPorAndar
	elevator?: number									// elevador
	elevatorLoad?: number							// elevadorCarga
	elevatorService?: number					// elevadorServico
	sideEntry?: boolean								// entradaLateral
	entryService?: boolean						// entradaServico
	entryVisitor?: boolean						// entradaVisitante
	parking?: number									// estacionamento
	coveredGarage?: number						// garagemCoberta
	garageCondominium?: number				// garagemCondominio
	garageDiscovery?: number					// garagemDescoberta
	closedGarage?: number							// garagemFechada
	bikeRack?: boolean								// bicicletario
	roomParty?: boolean								// salaoFesta
	gymRoom?: boolean									// salaGinastica
	roomGames?: boolean								// salaJogos
	tvRoom?: boolean									// salaTV
	playground?: boolean							// playground
	toyLibrary?: boolean							// brinquedoteca
	trackSkateboard?: boolean					// pistaSkate
	soccerField?: number							// campoFutebol
	heatedPool?: number								// piscinaAquecida
	privatePool?: number							// piscinaPrivativa
	childrensPool?: number						// piscinaInfantil
	multisportCourt?: boolean					// quadraPoliesportiva
	courtSquash?: boolean							// quadraSquash
	refectory?: boolean								// refeitorio
	boxGarageCovered?: number					// boxGaragemCoberta
	boxGarageDiscovery?: number				// boxGaragemDescoberta
	deposit?: number									// deposito
	privateDeposit?: number						// depositoPrivativo
	pipedGas?: boolean								// gasEncanado
	naturalGas?: boolean							// gasNatural
	generator?: number								// gerador
	hydrometer?: number								// hidrometro
	fireSystem?: boolean							// sistemaIncendio
	cableTV?: boolean									// tvCabo

	// city?: string										// cidade
	// neighborhood?: string							// bairro
	commercialNeighborhood?: string		// bairroComercial
	address?: string									// endereco
	// number?: string									// numero
	complement?: string								// complemento
	// zipCode?: string								// cep
	// state?: string									// estado
	localization?: string							// localizacao
	block?: string										// quadra
	batch?: string										// lote
	pointReference?: string						// pontoReferencia
	region?: string										// regiao
	uf?: string												// uf
	// country?: string								// pais

	statesList?: string


	owner?: string
	broker?: string

	ownerId?: string

	status?: PropertyStatus


	country?: string
	zipCode?: string
	state?: string
	city?: string
	neighborhood?: string[]
	addressLine?: string
	number?: string
	additionalInfo?: string

	location?: PropertyLocation
	geolocation?: GeoLocation

	values?: {
		rent: number,
		sellingValue: number,
		propertyValue: number,
		taxes: number,
		condominiumFee: number
	}

	images?: PropertyImage[]
	chips?: string[]

	createdBy?: string
	updatedBy?: string

	createdAt?: number
	updatedAt?: number
	deletedAt?: number
}

export interface PropertyCRM {
	// IDs
	id: number;
	idReferenciaCRM: string;
	idImobiliaria: number;

	// Imagens
	Imagens?: PropertyImageCRM[]

	// Imovel
	captador: string;
	dataCadastro: string;
	dataExclusao: string;
	contratoExclusividade: boolean;
	venda: boolean;
	locacao: boolean;
	permuta: boolean;
	imovelFinanciado: boolean;
	aceitaFinanciamento: number;
	imovelEmDestaque: boolean;
	tipoImovel: string;
	finalidade: string;
	anoConstrucao: number;
	dataReforma: number;
	faceDoImovel: string;
	qtdeSala: number;
	qtdeSalaEstar: number;
	metragemPeDireito: number;
	copa: boolean;
	cozinha: boolean;
	cozinhaAmericana: boolean;
	cozinhaDependente: boolean;
	cozinhaPlanejada: boolean;
	qtdeDormitorios: number;
	salaDeJantar: number;
	escritorio: number;
	qtdeBanheiro: number;
	lavabo: number;
	suite: number;
	closet: number;
	suiteCloset: number;
	suiteMaster: number;
	dormitorioEmpregada: number;
	banheiroEmpregada: number;
	lavanderia: number;
	vagasGaragem: number;
	areaLazer: number;
	churrasqueira: number;
	piscina: number;
	quintal: number;
	jardim: number;
	edicula: number;
	unidadeMedida: string;
	areaTerreno: number;
	areaConstruida: number;
	areaComum: number;
	areaServico: number;
	areaUtil: number;
	areaTotal: number;
	tituloImovel: string;
	subTituloImovel: string;
	descricaoImovel: string;
	tipoMoeda: string;
	valorSobConsulta: number;
	valorVenda: number;
	valorLocacao: number;
	valorCondominio: number;
	valorIPTU: number;
	valorAluguelTemporada: number;
	valorPromocional: number;
	precoMedioVenda: number;
	precoMedioLocacao: number;
	descricaoSEO: string;
	palavrasChaveSEO: string;
	estadoDoImovel: string;

	// Características
	reformado: boolean;
	mobiliado: boolean;
	semiMobiliado: boolean;
	moveisPlanejados: boolean;
	decorado: boolean;
	varanda: number;
	mezanino: number;
	sacada: number;
	sacadaFundo: number;
	sacadaLateral: number;
	arCondicionado: number;
	armarioSala: boolean;
	armarioBanheiro: boolean;
	armarioCloset: boolean;
	armarioCozinha: boolean;
	armarioDormitorio: boolean;
	porcelanato: boolean;
	carpete: boolean;
	contraPiso: boolean;
	granito: boolean;
	laminado: boolean;
	marmore: boolean;
	pisoFrio: boolean;
	pisoCeramica: boolean;
	taco: boolean;
	hidroMassagem: number;
	sauna: number;
	saunaPrivada: number;
	ofuro: number;
	spa: number;
	lareira: number;
	adega: number;
	bar: number;
	alarme: boolean;
	agua: boolean;
	aguaQuente: boolean;
	aquecedorCentral: boolean;
	aquecedorSolar: boolean;
	carpeteMadeira: boolean;
	circuitoTV: boolean;
	areaGourmet: number;
	murado: boolean;
	salaCinema: number;
	forno: number;
	casaCaseiro: number;
	redeAltaTensao: boolean;
	cachoeira: number;
	celeiro: number;
	cerca: boolean;
	curral: number;
	lago: number;
	pasto: boolean;
	pocoPeixe: number;
	reservatorioAgua: number;
	rio: boolean;
	transformador: boolean;

	// Edificio
	nomeConstrutora: string;
	nomeEdificio: string;
	nomeEmpreendimento: string;
	nomeCondominio: string;
	pagaCondominio: boolean;
	recepcao: boolean;
	portaria24Horas: boolean;
	guarita: boolean;
	portaoEletronico: boolean;
	interfone: boolean;
	hallEntrada: boolean;
	zelador: boolean;
	qtdeAndares: number;
	qtdeAptoPorAndar: number;
	elevador: number;
	elevadorCarga: number;
	elevadorServico: number;
	entradaLateral: boolean;
	entradaServico: boolean;
	entradaVisitante: boolean;
	estacionamento: number;
	garagemCoberta: number;
	garagemCondominio: number;
	garagemDescoberta: number;
	garagemFechada: number;
	bicicletario: boolean;
	salaoFesta: boolean;
	salaGinastica: boolean;
	salaJogos: boolean;
	salaTV: boolean;
	playground: boolean;
	brinquedoteca: boolean;
	pistaSkate: boolean;
	campoFutebol: number;
	piscinaAquecida: number;
	piscinaPrivativa: number;
	piscinaInfantil: number
	quadraPoliesportiva: boolean;
	quadraSquash: boolean;
	refeitorio: boolean;
	boxGaragemCoberta: number;
	boxGaragemDescoberta: number;
	deposito: number;
	depositoPrivativo: number;
	gasEncanado: boolean;
	gasNatural: boolean;
	gerador: number;
	hidrometro: number;
	sistemaIncendio: boolean;
	tvCabo: boolean;

	// Localizacação
	cidade: string;
	bairro: string[];
	bairroComercial: string;
	endereco: string;
	numero: string;
	complemento: string;
	cep: string;
	estado: string;
	localizacao: string;
	quadra: string;
	lote: string;
	pontoReferencia: string;
	regiao: string;
	uf: string;
	pais: string;
	latitude: string;
	longitude: string;
}

export type Properties = {} & Array<Property>

export interface searchPropertiesResponseCRM {
	temPaginacao: boolean
	qtdItens: number
	qtdTotalItens: number
	pagina: number
	temGeolocalizacao: boolean
	imoveis: PropertyCRM[]
	imoveisComGeolocalizacao: PropertyCRM[]
}

export interface StateLocations{
	uf?: string
	cidade?: string
}

export interface Land extends Property {
	area?: string
	value?: number
}

export interface House extends Property {
	landArea?: string
	value?: number
	constructedArea?: number
	bedrooms?: number
	suites?: number
	dinningRooms?: number
	livingRooms?: number
	restrooms?: number
	parkingSpaces?: number
	rentValue?: number
	saleValue?: number
	servicesFee?: number
	taxValue?: number

	// chips
	backyard?: boolean
	servicesArea?: boolean
	// pool?: boolean
	grill?: boolean
	// mezzanine?: boolean
}

export interface Loft extends Property {
	landArea?: string
	constructedArea?: number
	bedrooms?: number
	suites?: number
	dinningRooms?: number
	livingRooms?: number
	restrooms?: number
	backyard?: boolean
	parkingSpaces?: number
	saleValue?: number
	servicesFee?: number
	taxValue?: number
	quantityBathroom?: number

	// chips
	servicesArea?: boolean
	// pool?: boolean
	grill?: boolean
}

export interface Apartment extends Property {
	constructedArea?: number
	bedrooms?: number
	suites?: number
	dinningRooms?: number
	livingRooms?: number
	restrooms?: number
	parkingSpaces?: number
	saleValue?: number
	servicesFee?: number
	taxValue?: number

	// chips
	servicesArea?: boolean
	// pool?: boolean
	grill?: boolean
}

export interface Studio extends Property {
	constructedArea?: number
	bedrooms?: number
	dinningRooms?: number
	restrooms?: number
	backyard?: boolean
	parkingSpaces?: number
	saleValue?: number
	servicesFee?: number
	taxValue?: number

	// chips
	servicesArea?: boolean
	// pool?: boolean
	grill?: boolean
	playground?: boolean
	playroom?: boolean
	court?: boolean
	// mezzanine?: boolean
	roof?: boolean
}
