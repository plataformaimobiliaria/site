
export interface Rent {
  rent: number
  condominiumFee: number 
  taxes: number
} 

export interface Sale {
  sellingValue: number
  propertyValue: number
} 


export interface Chip {
  name: string
  value: string
  status: number
}