import { BusinessTypes, PropertyZones, PropertyTypes, Property } from '~/models/Property'

export interface ValueObject<T> {
	value: T
	text: string
}

export const getBusinessTypeRoute = (type: BusinessTypes): string => {
	switch (type) {
		case BusinessTypes.RENT:
			return 'alugar'
		case BusinessTypes.SALE:
			return 'comprar'
		default:
			return ''
	}
}

// Business Type
export const getBusinessType = (type: BusinessTypes): string => {
	switch (type) {
		case BusinessTypes.RENT:
			return 'Aluguel'
		case BusinessTypes.SALE:
			return 'Venda'
		default:
			return ''
	}
}
const getBusinessTypeValueObject = (businessType: BusinessTypes): ValueObject<BusinessTypes> => {
	return {
		value: businessType,
		text: getBusinessType(businessType)
	}
}
export const getBusinessTypeValueObjects = () => {
	return [BusinessTypes.RENT, BusinessTypes.SALE].map(businessType => getBusinessTypeValueObject(businessType))
}

// Property Types
export const getType = (type: PropertyTypes): string => {
	switch (type) {
		case PropertyTypes.APARTMENT:
			return 'Apartamento'
		case PropertyTypes.COUNTRYHOUSE:
			return 'Casa de Campo'
		case PropertyTypes.FARM:
			return 'Fazenda'
		case PropertyTypes.FARMSTEAD:
			return 'Chácara'
		case PropertyTypes.HOUSE:
			return 'Casa'
		case PropertyTypes.LAND:
			return 'Terreno'
		case PropertyTypes.LOFT:
			return 'Sobrado'
		default:
			return ''
	}
}
const getTypeValueObject = (type: PropertyTypes): ValueObject<PropertyTypes> => {
	return {
		value: type,
		text: getType(type)
	}
}
export const getPropertyTypeValueObjects = () => {
	return [
		PropertyTypes.APARTMENT,
		PropertyTypes.COUNTRYHOUSE,
		PropertyTypes.FARM,
		PropertyTypes.FARMSTEAD,
		PropertyTypes.HOUSE,
		PropertyTypes.LAND,
		PropertyTypes.LOFT
	].map(type => getTypeValueObject(type))
}

// Property Zones
export const getZone = (zone: PropertyZones): string => {
	switch (zone) {
		case PropertyZones.RURAL:
			return 'Rural'
		case PropertyZones.URBAN:
			return 'Urbana'
		default:
			return ''
	}
}
const getZoneValueObject = (zone: PropertyZones): ValueObject<PropertyZones> => {
	return {
		value: zone,
		text: getZone(zone)
	}
}
export const getZoneValueObjects = () => {
	return [PropertyZones.RURAL, PropertyZones.URBAN].map(zone => getZoneValueObject(zone))
}

export const getPropertyTitle = (property: Property) => {
	if (!property.type) {
		return ''
	}
	let title = firstUpperCase(`${getType(property.type) || property.type}`)
	if (property.neighborhood) {
		title += ` no ${property.neighborhood}`
	} else if (property.city) {
		title += ` em ${property.city}`
	}
	return title
}

const firstUpperCase = (input: string) => input.substring(0, 1).toUpperCase() + input.substring(1)
