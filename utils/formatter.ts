import { getBusinessType, getType, getZone } from "./property.utils"
import { BusinessTypes, PropertyTypes, PropertyZones } from "~/models/Property"

export const formatPriceRoundUp = (input: any, currency: string = '') => {
	const value = input ? parseFloat(input) : 0
	let price = (value / 1)
		.toFixed(0)
		.replace('.', ',')
		.toString()
		.replace(/\B(?=(\d{3})+(?!\d))/g, '.')
	if (currency && currency !== '') {
		price = `${currency} ${price}`
	}
	return price
}


export const formatPrice = function (input: string, currency: string) {
	const value = input ? parseFloat(input) : 0
	let price = (value / 1)
		.toFixed(2)
		.replace('.', ',')
		.toString()
		.replace(/\B(?=(\d{3})+(?!\d))/g, '.')
	if (currency) {
		price = `${currency} ${price}`
	}
	return price
}
export const firstUppercase = function (input: string) {
	return input.substring(0, 1).toUpperCase() + input.substring(1)
}
export const propertyType = (input: PropertyTypes) => getType(input)
export const businessType = (input: BusinessTypes) => getBusinessType(input)
export const propertyZone = (input: PropertyZones) => getZone(input)
export const timestampToDate = (input: number) => {
	const timestamp = input || Date.now()
	const date = new Date(timestamp)
	return `${date
		.getDate()
		.toString()
		.padStart(2, '0')}/${date
			.getMonth()
			.toString()
			.padStart(2, '0')}/${date.getFullYear()}`
}
export const formatingPrice = function (value: any) {
	if (!value) return ''
	value = parseInt(value).toString()
	return value.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
}