import { NuxtAxiosInstance } from '@nuxtjs/axios'
import Service from '@/service/SearchService'
import { BusinessTypes } from '~/models/Property'

export async function requestPropertyTypes($axios: NuxtAxiosInstance) {
  let propertyTypes = []
  try {
    const types = await $axios.request({
      method: 'GET',
      url: 'select-tipoimovel'
    })
    propertyTypes = types.data
  } catch (error) {
    console.log(error)
  }

  return propertyTypes
}

export async function requestStates(tipoNegocio: BusinessTypes, $axios: NuxtAxiosInstance) {
	let locacao = 0
	let venda = 0

	if (tipoNegocio === BusinessTypes.SALE) {
		venda = 1
	}

	if (tipoNegocio === BusinessTypes.RENT) {
		locacao = 1
	}

	let localizacoes = []
	try {
		const place = await $axios.request({
			method: 'GET',
			url: 'select-localimovel',
			params: {locacao , venda , geolocation: true},
		})
		localizacoes = place.data
	} catch (error) {
		console.log(error)
	}

	return localizacoes
}

export async function requestCities($axios: NuxtAxiosInstance){
	let localizacoes = []
	try {
		const place = await $axios.request({
			method: 'GET',
			url: 'select-localimovel',
		})
		localizacoes = place.data
	} catch (error) {
		console.log(error)
	}

	return localizacoes
}

export interface Cidade {
  cidade: string;
  bairros?: string[];
}

export interface StatesResponse {
  [sigla: string]: {
    [nome: string]: Cidade;
  }
}

export interface States {
  sigla: string,
  nome: string,
  cidades: Cidade[]
}

export const estadoSiglaParaNome: { [key: string]: string } = {
  'AC': 'Acre',
  'AL': 'Alagoas',
  'AP': 'Amapá',
  'AM': 'Amazonas',
  'BA': 'Bahia',
  'CE': 'Ceará',
  'DF': 'Distrito Federal',
  'ES': 'Espírito Santo',
  'GO': 'Goiás',
  'MA': 'Maranhão',
  'MT': 'Mato Grosso',
  'MS': 'Mato Grosso do Sul',
  'MG': 'Minas Gerais',
  'PA': 'Pará',
  'PB': 'Paraíba',
  'PR': 'Paraná',
  'PE': 'Pernambuco',
  'PI': 'Piauí',
  'RJ': 'Rio de Janeiro',
  'RN': 'Rio Grande do Norte',
  'RS': 'Rio Grande do Sul',
  'RO': 'Rondônia',
  'RR': 'Roraima',
  'SC': 'Santa Catarina',
  'SP': 'São Paulo',
  'SE': 'Sergipe',
  'TO': 'Tocantins'
}


export async function priceProperties($axios: NuxtAxiosInstance) {
	const preco = await Service.getSearchOptions($axios)

	const priceRange = [preco.valorVendaMin,  preco.valorVendaMax, preco.valorLocacaoMin, preco.valorLocacaoMax]

	if (!preco.valorVendaMin){		preco.valorVendaMin = 0	}
	if (!preco.valorVendaMax){		preco.valorVendaMax = preco.valorVendaMin	}
	if (!preco.valorLocacaoMin){		preco.valorLocacaoMin = 0	}
	if (!preco.valorLocacaoMax){		preco.valorLocacaoMax = preco.valorLocacaoMin	}

	return priceRange
}
