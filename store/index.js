export const state = () => ({
  toggleBusinessType: 0
})

export const mutations = {
  setToggleBusinessType(state, value) {
    state.toggleBusinessType = value
  }
}
