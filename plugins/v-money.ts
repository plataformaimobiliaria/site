import Vue from 'vue'
import money from 'v-money'

export default () => {
  Vue.use(money, {
    decimal: ',',
    thousands: '.',
    precision: 0,
  })
}
