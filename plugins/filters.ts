import Vue from 'vue'
import { businessType, firstUppercase, formatPrice, formatPriceRoundUp, propertyType, propertyZone, timestampToDate } from '@/utils/formatter'

export default () => {
	Vue.filter('formatPrice', formatPrice)
	Vue.filter('formatPriceRoundUp', formatPriceRoundUp)
	Vue.filter('firstUppercase', firstUppercase)
	Vue.filter('propertyType', propertyType)
	Vue.filter('businessType', businessType)
	Vue.filter('propertyZone', propertyZone)
	Vue.filter('timestampToDate', timestampToDate)
	Vue.filter('formatingPrice', formatPriceRoundUp)
}
