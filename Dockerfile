# Estágio de compilação
FROM node:16-alpine AS build

# Use ARG to define build arguments with default values
ARG BUILD_ENV=qa

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

COPY . .
RUN yarn build:${BUILD_ENV}

# Estágio final
FROM node:16-alpine

WORKDIR /usr/src/app

COPY --from=build /usr/src/app/dist ./dist
COPY --from=build /usr/src/app/.nuxt ./.nuxt
COPY package.json yarn.lock ./
RUN yarn install --production --frozen-lockfile

EXPOSE 8080

ENV HOST=0.0.0.0
ENV PORT=8080

CMD [ "yarn", "start" ]
